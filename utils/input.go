package utils

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// Input reads a line from stdin and returns it after trimming the
// trailing newline
func Input(prompt ...string) (in string, err error) {
	for _, p := range prompt {
		fmt.Printf("%s ", p)
	}
	fmt.Print("\b")

	reader := bufio.NewReader(os.Stdin)
	defer reader.Reset(os.Stdin)
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf(r.(string))
		}
	}()

	in, err = reader.ReadString('\n')

	if err != nil && err.Error() != "EOF" {
		panic(err)
	} else if err != nil && err.Error() == "EOF" {
		in = ""
		fmt.Println()
		return
	} else if IsInterrupted() {
		ResetCatch()
		in = ""
		err = fmt.Errorf("interrupted")
		return
	}

	in = strings.Trim(in, "\n\r")

	return
}

// InputLns gets a line of input from the user for each prompt
func InputLns(prompt ...string) (in []string, err error) {
	for _, p := range prompt {
		var ln string
		ln, err = Input(p)
		if err != nil {
			return
		}

		in = append(in, ln)
	}

	return
}

// Menu takes a slice of strings and prints them in a "menu" format,
// then prompts the user to choose an options by entering its number
func Menu(choices []string) (choice int) {
	if len(choices) == 0 {
		return -1
	}

	for i, v := range choices {
		fmt.Printf("%d) %s\n", i+1, v)
	}
	fmt.Println()

	for choice <= 0 || choice > len(choices) {
		in, err := Input("Choose: ")
		if err != nil {
			choice = -1
			return
		}

		choice, err = strconv.Atoi(in)
		if err != nil {
			choice = 0
		}
	}

	return
}
