package utils

import (
	"fmt"
	"os"
	"os/signal"
)

var catch = make(chan os.Signal, 1)
var interrupted = make(chan bool)

// StartCatch initializes (or resets) the catch and interrupted channels
// and starts a goroutine to wait for a SIGINT and write true to the
// interrupted channel once catched.
func StartCatch() {
	catch = make(chan os.Signal, 1)
	interrupted = make(chan bool)

	signal.Notify(catch, os.Interrupt)

	go func(c chan bool) {
		<-catch
		fmt.Print("\b\b  \b\b")
		c <- true
	}(interrupted)
}

// ResetCatch is an alias of StartCatch
func ResetCatch() { StartCatch() }

// IsInterrupted returns true if the interrupted channel is set to true,
// meaning a signal was catched, returns false otherwise.
func IsInterrupted() bool {
	select {
	case <-interrupted:
		return true
	default:
		return false
	}
}
