# FurAffinity Go

[![GoDoc](https://godoc.org/gitlab.com/MatteoCampinoti94/furaffinity-go?status.svg)](https://godoc.org/gitlab.com/MatteoCampinoti94/furaffinity-go)
[![pipeline status](https://gitlab.com/MatteoCampinoti94/furaffinity-go/badges/master/pipeline.svg)](https://gitlab.com/MatteoCampinoti94/furaffinity-go/commits/master)

A Go program that allows to save submissions and user-pages from FurAffinity locally.

This software is an evolution of my previous FA downloader, [FALocalRepo](https://gitlab.com/MatteoCampinoti94/FALocalRepo), which was written in Python.

You can download releases from the tags section of GitLab: [releases](https://gitlab.com/MatteoCampinoti94/furaffinity-go/tags).

**WARNING**: The program is currently in alpha and only has basic functionality.

## Current Features & Roadmap

* [x] Download users' sections
* [x] Update from database list of users
* [x] Download single submissions
* [x] Interrupt downloads
* [ ] Migration from [FALocalRepo](https://gitlab.com/MatteoCampinoti94/FALocalRepo)
* [ ] Search database
* [ ] Improved output

## Operating the program

The program has two types of inputs: menus and prompts.

The menus present choices with attached numbers:
```
1) Download
2) Exit

Choose:
```

To select an option type the corresponding number and press _ENTER_ (e.g. _1_ + _ENTER_)<br/>
Using _CTRL+C ENTER_ or _CTRL+D_ exits the current menu.

Prompts are expressed on a single line, enter text followed by _ENTER_.

```
Users:
```

Using _CTRL+C ENTER_ or _CTRL+D_ will exit the prompt, returning to the previous menu.

## Guide

When starting the program for the first time it will ask for a name to give to the database. An empty string (simply press _ENTER_) is allowed.

Currently, the main menu only has the Download option.

### Download

There are three options in the Download section:
* Users
* Update
* Submissions

### Users & Update

When selecting Users or Update the program will ask for three strings:
* Users<br/>
The usernames you want to download the gallery/scraps/favorites/extras of.
* Sections<br/>
The sections you want to download. These can be:
    * _g_ gallery
    * _s_ scraps
    * _f_ favorites
    * _E_ extras (looks for submissions that contain the username in the description, title or tags)
    * _e_ extras limited (looks for submissions that contain the link to the user)
* Options<br/>
There are only four options available currently:
    * _slow_ high download throttling, one get operation every 11 seconds
    * _quick_ minimum throttling, one get operations every second
    * _sync_ stop downloading a section when encountering a submission from that section that has already been downloaded
    * _quit_ exit the program at the end of all download operations

Once these commands are entered the program will proceed to download submissions from the sections of the inputted users.

If the Update option was chosen then the users and sections will be matched against the users already in the database and the sync option will be turned on by default.

The download is automatically throttled by limiting the number of operations that can be requested from FA. By default this is set to one operation every 6 seconds. The throttling can be changed with specific options (see above).

Submission files are saved in a tiered structured inside a folder called 'FA.files'. The submission ID is used to calculate its path.

```
001|2|345|678        Full ID
-------------------------------
  1| |   |           Top level
   |2|   |           Second level
   | |345|           Third level
-------------------------------
  1/2/345/0012345678 Final path
```

Submission metadata is saved in the database (see _The Database_ below).

Downloads can be interrupted with _CTRL+C_.

## The Database

The database (named 'FA.db') contains three tables:

1. `INFOS`<br>
The INFOS table contains information about the database.
    * `DBNAME`<br>
    Database custom name.
    * `VERSION`<br>
    Database version.
    * `USERN`<br>
    Number of users in `USERS` table.
    * `SUBN`<br>
    Number of submissions in the `SUBMISSIONS` table.
    * `LASTUP`<br>
    Time when the last update was started (epoch time, in seconds).
    * `LASTUPT`<br>
    Duration of last update (in seconds).
    * `LASTDL`<br>
    Time when the last download was started (epoch time).
    * `LASTDLT`<br>
    Duration of last download (in seconds).

2. `USERS`<br>
The USERS table contains a list of all the users that have been download with the program. Each entry contains the following:
    * `USER`<br>
    The URL username of the user (no caps and no underscores).
    * `USERFULL`<br>
    The full username as chosen by the user (with caps and underscores if present).
    * `FOLDERS`<br>
    The sections downloaded for that specific user.
    * `GALLERY`, `SCRAPS`, `FAVORITES`, `EXTRAS`<br>
    These contain a list of the submissions IDs downloaded for each section.


3. `SUBMISSIONS`<br>
The last table is a list of all the single submissions downloaded by the program. Each entry has 14 different values:
    * `ID`<br>
    The id of the submission
    * `AUTHOR`, `AUTHORURL`<br>
    The author username in normal format and URL format (e.g. 'Flying_Tiger' and 'flyingtiger')
    * `TITLE`<br>
    The title
    * `UDATE`<br>
    Upload date
    * `DESCRIPTION`<br>
    Description in HTML format.
    * `TAGS`<br>
    The submission's keywords sorted alphanumerically.
    * `CATEGORY`, `SPECIES`, `GENDER`, `RATING`<br>
    The category, species, gender and rating as listed on the submission's page on the forum (still WIP).
    * `FILELINK`, `FILENAME`<br>
    The link to the submission file on the forum and the name of the downloaded file (all files are named 'submission' + their proper extension) (an empty or 0 value means the file has an error on the forum and has not been downloaded).
    * `LOCATION`<br>
    The location in the current folder of the submission's file
    * `ONSERVER`<br>
    This last field is defaulted to 1 and only updated to 0 if the program checks the submission on the forum and finds it missing (because the uploaded has either disabled or deleted it).

The database is built using SQLite3 so it can be easily opened and searched with a vast number of third-party programs. A good one is 'DB Browser for SQLite' (http://sqlitebrowser.org/) which is open source, cross-platform and has a project page on GitHub.

## Build & Run

```sh
git clone https://gitlab.com/MatteoCampinoti94/furaffinity-go.git
cd furaffinity-go
go get ./...
go build main.go
./main
```

Alternatively:

```sh
go get gitlab.com/MatteoCampinoti94/furaffinity-go
cd "$GOPATH"/src/gitlab.com/MatteoCampinoti94/furaffinity-go
go build main.go
./main
```

# Appendix

For more information on some of the functions used by the program, refer to the godoc.org page: [GoDoc furaffinity-go](https://godoc.org/gitlab.com/MatteoCampinoti94/furaffinity-go).
