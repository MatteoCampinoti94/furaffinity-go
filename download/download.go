package download

import (
	"database/sql"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/MatteoCampinoti94/furaffinity-go/database"
	"gitlab.com/MatteoCampinoti94/furaffinity-go/utils"

	faapi "gitlab.com/MatteoCampinoti94/faapi-go"
)

type dlOpts struct {
	speed int8
	sync  bool
	exit  bool
}

// parseOptions parses a string and returns a struct with options fields
func parseOptions(optIn string) (optOut dlOpts) {
	optIn = strings.ToLower(optIn)

	if strings.Contains(optIn, "quick") {
		optOut.speed = 2
	} else if strings.Contains(optIn, "slow") {
		optOut.speed = 0
	} else {
		optOut.speed = 1
	}
	optOut.sync = strings.Contains(optIn, "sync")
	optOut.exit = strings.Contains(optIn, "quit")

	return
}

// cleanSections cleans up a sections string
func cleanSections(sectIn string) (sectOut string) {
	re, _ := regexp.Compile("[^gsfE]")

	sectIn = re.ReplaceAllString(sectIn, "")

	for _, c := range sectIn {
		if !strings.Contains(sectOut, string(c)) {
			sectOut += string(c)
		}
	}

	return
}

// cleanUsers cleans up a users slice
func cleanUsers(usersIn []string) (usersOut []string) {
	for _, usr := range usersIn {
		usr = database.UserURL(usr)
		if len(usr) == 0 {
			continue
		}

		match := func(uIn []string, match string) bool {
			for _, u := range uIn {
				if u == match {
					return true
				}
			}
			return false
		}(usersOut, usr)

		if !match {
			usersOut = append(usersOut, usr)
		}
	}

	return
}

// NewAPI creates a new FAAPI instance loading cookies in it
func NewAPI(cookiesPath string) (api faapi.FAAPI, err error) {
	err = api.LoadCookies(cookiesPath)
	if err != nil {
		return
	}

	err = api.MakeRequest()
	if err != nil {
		return
	}

	return
}

// downloadSubmission downloads single submissions in the database
func downloadSubmissions(api faapi.FAAPI, db *sql.DB, ids []string, opts dlOpts) error {
	switch opts.speed {
	case 0:
		api.Interval = time.Second * 11
	case 1:
		api.Interval = time.Second * 5
	case 2:
		api.Interval = time.Second * 1
	}
	defer func(api *faapi.FAAPI) {
		api.Interval = time.Second * 0
	}(&api)
	defer utils.ResetCatch()

	for _, id := range ids {
		if utils.IsInterrupted() {
			return nil
		}

		var sub faapi.Submission
		var err error

		fmt.Printf("Download sub %10s ... ", id)
		sub, err = api.GetSubmission(id)
		if err != nil {
			fmt.Printf("ERROR: %v\n", err)
		} else {
			fmt.Println("Done")
		}

		err = sub.GetFile()
		if err != nil {
			fmt.Printf("ERROR: %v\n", err)
			continue
		}

		err = database.AddSubmission(db, sub, false)
		if err != nil {
			fmt.Printf("ERROR: %v\n", err)
		}
	}

	return nil
}

// downloadUsers handles downloading user galleries, scraps, etc...
func downloadUsers(api faapi.FAAPI, db *sql.DB, usersAndSections [][]string, opts dlOpts) error {
	switch opts.speed {
	case 0:
		api.Interval = time.Second * 11
	case 1:
		api.Interval = time.Second * 5
	case 2:
		api.Interval = time.Second * 1
	}
	defer func(api *faapi.FAAPI) {
		api.Interval = time.Second * 0
	}(&api)
	defer utils.ResetCatch()

	for _, usrSect := range usersAndSections {
		if utils.IsInterrupted() {
			break
		}

		user := usrSect[0]
		sections := usrSect[1]
		// TODO: check if user exists
		err := database.AddUser(db, user)
		if err != nil {
			fmt.Printf("ERROR: %v\n", err)
			continue
		}

		for _, sect := range sections {
			if utils.IsInterrupted() {
				break
			}

			var sync bool

			err := database.UpdateUserAdd(db, user, "FOLDERS", string(sect))
			if err != nil {
				fmt.Printf("ERROR: %v\n", err)
				continue
			}

			for page, pageIdx := 1, 1; page > 0 && !sync; pageIdx++ {
				if utils.IsInterrupted() {
					break
				}

				var subs []faapi.Submission
				var err error
				var dbFolder string

				fmt.Printf("Download page %d ... ", pageIdx)

				switch sect {
				case 'g':
					dbFolder = "GALLERY"
					subs, page, err = api.GetGallery(user, page)
				case 's':
					dbFolder = "SCRAPS"
					subs, page, err = api.GetScraps(user, page)
				case 'f':
					dbFolder = "FAVORITES"
					subs, page, err = api.GetFavorites(user, page)
				case 'e':
					dbFolder = "EXTRAS"
					var searchParams = map[string]string{
						"q":               fmt.Sprintf(`@message (":icon%s:" | ":%sicon:") ! (@lower %s)`, user, user, user),
						"order-by":        "date",
						"order-direction": "desc",
					}
					subs, page, err = api.GetSearch(searchParams, page)
				case 'E':
					dbFolder = "EXTRAS"
					var searchParams = map[string]string{
						"q":               fmt.Sprintf(`(@message (":icon%s:" | ":%sicon:" | "%s")) | (@keywords "%s") | (@title "%s") ! (@lower %s)`, user, user, user, user, user, user),
						"order-by":        "date",
						"order-direction": "desc",
					}
					subs, page, err = api.GetSearch(searchParams, page)
				}

				if err != nil {
					fmt.Printf("ERROR: %v\n", err)
				} else {
					fmt.Println("Done")
				}

				for _, sub := range subs {
					if utils.IsInterrupted() {
						break
					}

					id := strconv.Itoa(sub.ID)

					fmt.Printf("Download sub %10s ... ", id)

					if f, err := database.FindUserValue(db, user, dbFolder, id); err != nil {
						fmt.Printf("ERROR: %v\n", err)
					} else if f {
						fmt.Println("Already in database [subs,usrs]")
						if opts.sync {
							sync = true
							break
						}
						continue
					}

					if s, err := database.GetSubmission(db, sub.ID); err != nil {
						fmt.Printf("ERROR: %v\n", err)
					} else if s.ID == sub.ID {
						fmt.Println("Already in database [subs]")
					} else {
						sub, err = api.GetSubmission(strconv.Itoa(sub.ID))
						if err != nil {
							fmt.Printf("ERROR: %v\n", err)
						} else {
							fmt.Println("Done")
						}

						err = sub.GetFile()
						if err != nil {
							fmt.Printf("ERROR: %v\n", err)
						}

						err = database.AddSubmission(db, sub, true)
						if err != nil {
							fmt.Printf("ERROR: %v\n", err)
							continue
						}
					}

					database.UpdateUserAdd(db, user, dbFolder, strconv.Itoa(sub.ID))
					if err != nil {
						fmt.Printf("ERROR: %v\n", err)
					}
				}
			}
		}
	}

	return nil
}

// Download is the user-facing function that handles downloads
func Download(api faapi.FAAPI, db *sql.DB) (quit bool) {
	menuDownload := []string{
		"Users",
		"Update",
		"Submissions",
		"Exit",
	}

	for true {
		opt := utils.Menu(menuDownload)

		switch opt {
		case 1:
			in, err := utils.InputLns("Users: ", "Sections: ", "Options: ")
			if err != nil {
				break
			}

			usrs := cleanUsers(strings.Split(in[0], " "))
			sect := cleanSections(in[1])
			opts := parseOptions(in[2])

			usrsSect := [][]string{}
			for _, usr := range usrs {
				usrsSect = append(usrsSect, []string{usr, sect})
			}

			t1 := time.Now()

			downloadUsers(api, db, usrsSect, opts)

			t2 := time.Now()

			database.UpdateInfo(db, "LASTDL", strconv.Itoa(int(t1.Unix())))
			database.UpdateInfo(db, "LASTDLT", strconv.Itoa(int(t2.Sub(t1).Seconds())))
			database.UpdateInfoTotals(db)

			if opts.exit {
				return true
			}
		case 2:
			in, err := utils.InputLns("Users: ", "Sections: ", "Options: ")
			if err != nil {
				break
			}

			usrs := cleanUsers(strings.Split(in[0], " "))
			sect := cleanSections(in[1])
			opts := parseOptions(in[2])
			opts.sync = true

			if len(usrs) == 0 {
				usrs = []string{"%"}
			}
			if len(sect) == 0 {
				sect = "gsfeE"
			}

			usrsDB, err := database.GetUser(db, "USER,FOLDERS", usrs...)
			re, _ := regexp.Compile(fmt.Sprintf("[^%s]", sect))

			usrsSect := [][]string{}
			for _, usr := range usrsDB {
				u := usr[0][0]
				s := usr[1][0]
				s = re.ReplaceAllString(s, "")
				usrsSect = append(usrsSect, []string{u, s})
			}

			t1 := time.Now()

			downloadUsers(api, db, usrsSect, opts)

			t2 := time.Now()

			database.UpdateInfo(db, "LASTUP", strconv.Itoa(int(t1.Unix())))
			database.UpdateInfo(db, "LASTUPT", strconv.Itoa(int(t2.Sub(t1).Seconds())))
			database.UpdateInfoTotals(db)

			if opts.exit {
				return true
			}
		case 3:
			in, err := utils.InputLns("IDs: ", "Options: ")
			if err != nil {
				break
			}

			ids := strings.Split(in[0], " ")
			opts := parseOptions(in[1])

			t1 := time.Now()

			downloadSubmissions(api, db, ids, opts)

			t2 := time.Now()

			database.UpdateInfo(db, "LASTDL", strconv.Itoa(int(t1.Unix())))
			database.UpdateInfo(db, "LASTDLT", strconv.Itoa(int(t2.Sub(t1).Seconds())))
			database.UpdateInfoTotals(db)

			if opts.exit {
				return true
			}
		case len(menuDownload), -1:
			return
		}
	}

	return
}
