package main

import (
	"gitlab.com/MatteoCampinoti94/furaffinity-go/database"
	"gitlab.com/MatteoCampinoti94/furaffinity-go/download"
	"gitlab.com/MatteoCampinoti94/furaffinity-go/utils"
)

func main() {
	var menuMain = []string{
		"Download",
		"Exit",
	}

	utils.StartCatch()

	db, err := database.OpenDatabase("FA.db")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	api, err := download.NewAPI("FA.cookies.json")
	if err != nil {
		panic(err)
	}

	for true {
		switch utils.Menu(menuMain) {
		case 1:
			quit := download.Download(api, db)
			if quit {
				return
			}
		case len(menuMain), -1:
			return
		}
	}
}
