package database

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	faapi "gitlab.com/MatteoCampinoti94/faapi-go"

	"github.com/h2non/filetype"
	"github.com/h2non/filetype/types"
)

// submissionLocation returns a string with the tiered submission id
func submissionLocation(id int) (location string) {
	var t1, t2, t3 = 10000000, 1000000, 1000

	tier1 := id / t1
	tier2 := (id - (t1 * tier1)) / t2
	tier3 := ((id - (t1 * tier1)) - (t2 * tier2)) / t3

	location = fmt.Sprintf("%d/%d/%03d", tier1, tier2, tier3)

	return
}

// AddSubmission add submission to database
func AddSubmission(db *sql.DB, sub faapi.Submission, overwrite bool) (err error) {
	if overwrite == true {
		err = RemoveSubmission(db, sub.ID)
		if err != nil {
			return
		}
	}

	var path, ext, subname string

	path = fmt.Sprintf("%s/%010d", submissionLocation(sub.ID), sub.ID)

	if len(sub.File) != 0 {
		ext, err = writeSubmission(sub.ID, sub.FileURL, path, sub.File)
		subname = "submission." + ext
		if err != nil {
			return
		}
	} else {
		subname = "0"
	}

	_, err = db.Exec(`INSERT INTO SUBMISSIONS
	(ID,AUTHOR,AUTHORURL,TITLE,UDATE,DESCRIPTION,TAGS,CATEGORY,SPECIES,GENDER,RATING,FILELINK,FILENAME,LOCATION,ONSERVER)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		sub.ID, sub.Author, UserURL(sub.Author), sub.Title,
		sub.Date, sub.Description, strings.Join(sub.Tags, " "), "wip",
		"wip", "wip", "wip", sub.FileURL,
		subname, path, 1)
	return
}

// GetSubmission returns a submission as a faapi.Submission struct (if found)
func GetSubmission(db *sql.DB, id int) (sub faapi.Submission, err error) {
	var res [][]interface{}
	res, err = query(db, "SELECT * FROM SUBMISSIONS WHERE ID = ?", id)

	if len(res) == 0 {
		return
	}

	subRaw := res[0]
	sub.ID = int(subRaw[0].(int64))
	sub.Author = subRaw[1].(string)
	sub.Title = subRaw[3].(string)
	sub.Date = subRaw[4].(string)
	sub.Description = subRaw[5].(string)
	sub.Tags = strings.Split(subRaw[6].(string), " ")
	sub.FileURL = subRaw[11].(string)

	return
}

// RemoveSubmission removes a submission given its ID
func RemoveSubmission(db *sql.DB, id int) (err error) {
	_, err = db.Exec(`DELETE FROM SUBMISSIONS WHERE ID = ?`, id)
	return
}

// writeSubmission writes a submission file to its corresponding directory
func writeSubmission(id int, url string, path string, file []byte) (ext string, err error) {
	var t types.Type

	t, err = filetype.Get(file)
	if err != nil {
		return
	}

	ext = t.Extension

	if t.MIME.Type == "" {
		ext = ""
		tmp := strings.Split(url, ".")
		if len(tmp) == 0 {
			return
		}
		ext = tmp[len(tmp)-1]
	}

	os.MkdirAll("FA.files/"+path, 0755)

	err = ioutil.WriteFile("FA.files/"+path+"/submission."+ext, file, 0644)

	return
}
