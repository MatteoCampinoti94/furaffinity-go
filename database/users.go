package database

import (
	"database/sql"
	"fmt"
	"regexp"
	"sort"
	"strings"
)

// UserURL changes the username to the URL version
func UserURL(usrIn string) (userOut string) {
	re, _ := regexp.Compile("[^A-Za-z0-9.\\-~]")

	userOut = re.ReplaceAllString(usrIn, "")
	userOut = strings.ToLower(userOut)

	return
}

// AddUser adds a user to the database
func AddUser(db *sql.DB, username string) (err error) {
	check, err := GetUser(db, "USER", username)
	if err != nil {
		return err
	} else if len(check) != 0 {
		return
	}

	_, err = db.Exec(`INSERT INTO USERS
	(USER,USERFULL,FOLDERS,GALLERY,SCRAPS,FAVORITES,EXTRAS)
	VALUES (?,?,?,?,?,?,?)`,
		UserURL(username), username, "", "", "", "", "")

	return
}

// UpdateUserAdd writes content to a user's column
func UpdateUserAdd(db *sql.DB, username string, column string, value string) (err error) {
	col, err := GetUser(db, column, username)
	if err != nil {
		return err
	} else if len(col) == 0 {
		return fmt.Errorf("Database error: user not found")
	}

	colVals := col[0][0]

	if len(colVals) == 0 || len(colVals) == 1 && colVals[0] == "" {
		colVals = []string{value}
	} else {
		for _, v := range colVals {
			if v == value {
				return
			}
		}

		colVals = append(colVals, value)

		sort.Strings(colVals)
	}

	colOut := strings.Join(colVals, ",")

	_, err = db.Exec(fmt.Sprintf(`UPDATE USERS SET %s = ? WHERE USER = ?`, column), colOut, username)

	return
}

// FindUserValue finds a value in a user column
func FindUserValue(db *sql.DB, username string, column string, value string) (found bool, err error) {
	var col [][][]string
	col, err = GetUser(db, column, username)
	if err != nil {
		return
	} else if len(col) == 0 {
		err = fmt.Errorf("Database error: user not found")
		return
	}

	colVals := col[0][0]

	if l := len(colVals); l == 0 || l == 1 && colVals[0] == "" {
		return
	}

	for _, v := range colVals {
		if v == value {
			found = true
			break
		}
	}

	return
}

// GetUser returns a slice of column values for any number of users
func GetUser(db *sql.DB, column string, username ...string) (data [][][]string, err error) {
	for _, usr := range username {
		res, err := query(db, fmt.Sprintf("SELECT %v FROM USERS WHERE USER LIKE ?", column), usr)
		if err != nil {
			return nil, err
		} else if len(res) == 0 {
			continue
		}

		rows := [][][]string{}

		for _, rowRaw := range res {
			row := [][]string{}
			for _, colRaw := range rowRaw {
				col := strings.Split(colRaw.(string), ",")
				row = append(row, col)
			}
			rows = append(rows, row)
		}

		data = append(data, rows...)
	}

	return
}
