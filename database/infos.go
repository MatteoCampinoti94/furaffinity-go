package database

import (
	"database/sql"
	"strconv"
)

// UpdateInfo updates a field in the INFOS table
func UpdateInfo(db *sql.DB, field string, value string) (err error) {
	_, err = db.Exec("UPDATE INFOS SET VALUE = ? WHERE FIELD = ?", value, field)
	return
}

// UpdateInfoTotals updates the totals in the INFOS tables: USRN, SUBN
func UpdateInfoTotals(db *sql.DB) (err error) {
	usrnRaw, err := query(db, "SELECT COUNT(USER) FROM USERS")
	if err != nil {
		return
	}

	subnRaw, err := query(db, "SELECT COUNT(ID) FROM SUBMISSIONS")
	if err != nil {
		return
	}

	usrn := int(usrnRaw[0][0].(int64))
	subn := int(subnRaw[0][0].(int64))

	err = UpdateInfo(db, "USRN", strconv.Itoa(usrn))
	if err != nil {
		return
	}

	err = UpdateInfo(db, "SUBN", strconv.Itoa(subn))
	if err != nil {
		return
	}

	return
}
