package database

import (
	"database/sql"
	"fmt"
	"os"

	"gitlab.com/MatteoCampinoti94/furaffinity-go/utils"

	// Empty import of sqlite3 driver
	_ "github.com/mattn/go-sqlite3"
)

var version = "0.1.4alpha"

var tableInfos = `
CREATE TABLE IF NOT EXISTS INFOS (
	FIELD TEXT,
	VALUE TEXT
);`

var tableUsers = `
CREATE TABLE IF NOT EXISTS USERS (
	USER TEXT UNIQUE PRIMARY KEY NOT NULL,
	USERFULL TEXT NOT NULL,
	FOLDERS TEXT NOT NULL,
	GALLERY TEXT,
	SCRAPS TEXT,
	FAVORITES TEXT,
	EXTRAS TEXT
);`

var tableSubmissions = `
CREATE TABLE IF NOT EXISTS SUBMISSIONS (
	ID INT UNIQUE PRIMARY KEY NOT NULL,
	AUTHOR TEXT NOT NULL,
	AUTHORURL TEXT NOT NULL,
	TITLE TEXT,
	UDATE CHAR(10) NOT NULL,
	DESCRIPTION TEXT,
	TAGS TEXT,
	CATEGORY TEXT,
	SPECIES TEXT,
	GENDER TEXT,
	RATING TEXT,
	FILELINK TEXT,
	FILENAME TEXT,
	LOCATION TEXT NOT NULL,
	ONSERVER INT
);`

// query queries the database and returns a generalised slice of rows
func query(db *sql.DB, queryStr string, queryArgs ...interface{}) (container [][]interface{}, err error) {
	var rows *sql.Rows

	rows, err = db.Query(queryStr, queryArgs...)
	if err != nil {
		return
	}
	defer rows.Close()

	cols, _ := rows.Columns()

	for rows.Next() {
		var row = make([]interface{}, len(cols))
		var rowPtrs = row

		for i := range row {
			rowPtrs[i] = &row[i]
		}

		err = rows.Scan(rowPtrs...)
		if err != nil {
			return
		}

		container = append(container, row)
	}

	return
}

// OpenDatabase opens the database in path, if present
func OpenDatabase(path string) (db *sql.DB, err error) {
	if _, errDB := os.Stat(path); errDB == nil {
		db, err = sql.Open("sqlite3", path)
		if err != nil {
			return nil, err
		} else if db == nil {
			return nil, fmt.Errorf("Database is nil")
		}

		err = checkTables(db)
		if err != nil {
			return nil, err
		}

		err = UpdateInfo(db, "VERSION", version)
		if err != nil {
			return nil, err
		}

		return
	} else if os.IsNotExist(errDB) {
		fmt.Println("Creating new database")
		name, err := utils.Input("What will the database name be? ")
		if err != nil {
			return nil, err
		}

		db, err = newDatabase(name, path)
	} else {
		panic(err)
	}

	return
}

// newDatabase creates a new database with the provided path and adds INFOS,
// USERS and SUBMISSIONS tables. name is added to the aforementioned INFOS table.
func newDatabase(name string, path string) (db *sql.DB, err error) {
	db, err = sql.Open("sqlite3", path)
	if err != nil {
		return nil, err
	} else if db == nil {
		return nil, fmt.Errorf("Database is nil")
	}

	_, err = db.Exec(tableInfos + tableUsers + tableSubmissions)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(fmt.Sprintf(`
	INSERT INTO INFOS (FIELD, VALUE) VALUES ("DBNAME", "%s");
	INSERT INTO INFOS (FIELD, VALUE) VALUES ("VERSION", "%s");
	INSERT INTO INFOS (FIELD, VALUE) VALUES ("USRN", 0);
	INSERT INTO INFOS (FIELD, VALUE) VALUES ("SUBN", 0);
	INSERT INTO INFOS (FIELD, VALUE) VALUES ("LASTUP", 0);
	INSERT INTO INFOS (FIELD, VALUE) VALUES ("LASTUPT", 0);
	INSERT INTO INFOS (FIELD, VALUE) VALUES ("LASTDL", 0);
	INSERT INTO INFOS (FIELD, VALUE) VALUES ("LASTDLT", 0);`, name, version))
	if err != nil {
		return nil, err
	}

	return
}

// checkDatabase makes sure the database is formatted in the correct way
func checkTables(db *sql.DB) error {
	for _, table := range []string{"INFOS", "USERS", "SUBMISSIONS"} {
		res, err := query(db, `SELECT EXISTS(SELECT name FROM sqlite_master WHERE type = "table" AND name = ?)`, table)
		if err != nil {
			return err
		}

		if t, ok := res[0][0].(int64); ok && t != 1 {
			return fmt.Errorf("%s table is not present", table)
		}
	}

	return nil
}
